#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='pocketapi',
            version='0.1',
            description='Python module to interact with the Pocket API',
            author='Jeremy Derr',
            author_email='jeremy@derr.me',
            url='http://www.endlessfusion.com/projects/pocketapi',
            packages=find_packages(),
           )


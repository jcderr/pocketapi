import unittest
import pocketapi
from local_settings import CONSUMER_KEY as ckey


class TestPocketAPI(unittest.TestCase):
    def setUp(self):
        self.pocket = pocketapi.Pocket(ckey, 'http://endlessfusion.com')

    def testGet(self):
        self.assertTrue(len(self.pocket.retreive(tags=['doozer'])) > 1)


if __name__ == '__main__':
    unittest.main()

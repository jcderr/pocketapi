import json
import urllib2
import webbrowser

class Pocket(object):
    def __init__(self, consumerkey, accesstoken, siteurl):
        self.req_url = 'https://getpocket.com/v3/oauth/request'
        self.auth_url = 'https://getpocket.com/v3/oauth/authorize'
        self.retr_url = 'https://getpocket.com/v3/get'
        self.mod_url = None
        self.add_url = None
        self.consumer_key = consumerkey
        self.access_token = accesstoken
        self.site_url = siteurl
        self.headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'X-Accept': 'application/json'
        }
        if not self.access_token:
            self.token = self.request_token()
            self.access_token, self.username = self.request_authorization()

    def request_token(self):
        request_data = json.dumps({
            "consumer_key": self.consumer_key,
            "redirect_uri": self.site_url,
            })
        try:
            data = self.makerequest(self.req_url, request_data)
            return data['code']
        except Exception, e:
            raise Exception(e)

    def request_authorization(self):
        import time
        redir_url = 'https://getpocket.com/auth/authorize?request_token={}&redirect_uri={}'
        webbrowser.open(redir_url.format(self.token, self.site_url))
        time.sleep(10)

        request_data = json.dumps({
            "consumer_key": self.consumer_key,
            "code": self.token,
            })
        try:
            data = self.makerequest(self.auth_url, request_data)
            return data['access_token'], data['username']
        except Exception, e:
            raise e


    def retrieve(self, **kwargs):
        if not self.access_token:
            self.auth()
        request_data = {
            "consumer_key": self.consumer_key,
            "access_token": self.access_token,
        }
        for kw in kwargs:
            if kw is 'tag':
                request_data['tag'] = kwargs[kw]
            if kw is 'since':
                request_data['since'] = kwargs[kw]
        request_data = json.dumps(request_data)
        data = self.makerequest(self.retr_url,
                                request_data)
        return data['list']

    def add(self, link):
        pass

    def modify(self, obj):
        pass

    def makerequest(self, url, data):
        request = urllib2.Request(url, data, self.headers)
        response = urllib2.urlopen(request)
        return json.load(response)
